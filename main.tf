# secrets/main

resource "random_password" "generated_password" {
  for_each = var.secrets
  length   = 16
  special  = false
}

resource "aws_secretsmanager_secret" "secret" {
  for_each = var.secrets
  name     = each.key
  tags = {
    ManagedBy   = var.managed_by
    Environment = var.environment
  }
}

resource "aws_secretsmanager_secret_version" "secret_version" {
  for_each      = var.secrets
  secret_id     = aws_secretsmanager_secret.secret[each.key].id
  secret_string = "x${random_password.generated_password[each.key].result}"
}

resource "aws_secretsmanager_secret" "predefined_secret" {
  for_each = var.predefined_secrets
  name     = each.key
  tags = {
    ManagedBy   = var.managed_by
    Environment = var.environment
  }
}

resource "aws_secretsmanager_secret_version" "predefined_secret_version" {
  for_each      = var.predefined_secrets
  secret_id     = aws_secretsmanager_secret.predefined_secret[each.key].id
  secret_string = each.value
}

