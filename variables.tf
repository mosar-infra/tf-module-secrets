variable "secrets" {
  default = {}
}
variable "predefined_secrets" {
  default = {}
}
variable "environment" {}
variable "managed_by" {}
