# Tf Module Secrets

Created secrets either by generating passwords for variable `secrets` or using secrets values passed in for `predefined_secrets`.
Expects variables `environment` and `managed_by` as well.

Example for secrets:
```
secrets = {
  db_rootPassword = {}
  db_password = {}
}
```

Example for predefined_secrets:

```
predefined_secrets = {
  db_creds = {
    USER = "someUsername"
    PASSWORD = "someExistingPass"
  }
}
```
