# secrets outputs

output "secrets" {
  value = aws_secretsmanager_secret.secret
}
output "secrets_versions" {
  value = aws_secretsmanager_secret_version.secret_version
}
output "predefined_secrets" {
  value = aws_secretsmanager_secret.predefined_secret
}
output "predefined_secret_versions" {
  value = aws_secretsmanager_secret_version.predefined_secret_version
}
